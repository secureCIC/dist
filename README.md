# CasesInCompensation_dist
Executable distribution of the cases in compensation (CIC) project. For more information check http://casesincompensation.com/

###########################################################################
# Description
In order to use CIC project you need two components: CIC code and CIC database. CIC code components are freely avaibale and you can access its publicly available source code on https://github.com/proska/CasesInCompensation. CIC database, consist of propritary databases necessary to follow the exercise in your textbook. This part will be generated per user and it can only be used by one person. 

###########################################################################
# Instructions to download:
  1. download "genSeed", "PhaseII", and "PhaseIII" executable based on your operating system. (e.g. genSeed_win.exe for windows users)
  2. run the "genSeed" on your personal system (or the system that you want to user CIC on it).
  3. after running "genSeed" a small file "seed" will be created on you system. 
  4. email generated "seed" file to us with your username and password information. 
  5. We will send you the encrypted database that can only be accessed on you system.
  6. this encrypted database consists of two folders "PhaseII" and "Phase III". You need to copy the contents of each folder and paste tham inside the "DBs" folder of respective CIC code component.
  7. You should be able to run phaseII and phaseIII by clicking on the executable in their respective folder.
  
###########################################################################
# reporting bugs
Althougha a lot of effort has been made to make sure the software is working properly, software bugs and errors are inevitable. If you faced any such a problem and no clear error message was given to you. please inform us via email to : qasemi.ehs@gmail.com with subject line "[CIC] Issue Report" (or just report an issue by clicking "Issues" on top of this repository).

Please note that, sending the details of the scenario that resulted in error will help us to pipoint the problematic part of software easily. So make sure you include as much details as possible in your issue reports or emails. Thanks!

###########################################################################
Finally please support us by clicking on the "start" icon above. 
Thanks Ehsan (Eric)

my website: http://ehsanqasemi.com
